# Greenioty SPH Exhibition

![Greenioty SPH Exhibition](./documentation/greenioty-exhibition-sph.jpg)


**Original exhibit build and inspired by greenioty. More info on greenioty.com**

In this repo you find a collection of hardware designs and software to build and maintain the greenioty exhibition.

- Hardware: `/hardware`
  - Currently a 3D printable cap to fill holes without plants
- Software: `...`

# License

MIT