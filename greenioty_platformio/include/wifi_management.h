#pragma once

#include "greenioty_mqtt.h"
#include "greenioty_schedule.h"
#include <WiFi.h>
#include <WiFiManager.h>
#include <Preferences.h>

// Create a Preferences object to store the configuration parameters persistently
extern Preferences greeniotyPrefs;

// WiFi Configuration Portal Parameters
extern const char* config_ssid;
extern const char* config_pass;

// Create global instances of WiFiManager
extern WiFiManager wifiManager;

// Create custom parameters for the Wi-Fi configuration
extern WiFiManagerParameter custom_light_on_hour;
extern WiFiManagerParameter custom_light_off_hour;
extern WiFiManagerParameter custom_pump_interval;
extern WiFiManagerParameter custom_pump_duration;
extern WiFiManagerParameter custom_mqtt_server;
extern WiFiManagerParameter custom_mqtt_port;
extern WiFiManagerParameter custom_mqtt_get_state_topic;
extern WiFiManagerParameter custom_mqtt_set_schedule_topic;
extern WiFiManagerParameter custom_mqtt_user;
extern WiFiManagerParameter custom_mqtt_pass;
extern WiFiManagerParameter custom_mqtt_publish_interval;

// Function declarations
void WiFiConfigInit();
void saveConfigCallback();
void loadConfig();
void updateSchedulePersistant();