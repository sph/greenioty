#pragma once

#include <Arduino.h>
#include "wifi_management.h"
#include "relay_module.h"
#include "greenioty_schedule.h"
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// MQTT Parameters
extern String mqttServer;
extern int mqttPort;
extern String mqttUser;
extern String mqttPassword;
extern const char* mqttClientID;
// MQTT topic to publish water level, pump status, and light status
extern String greeniotyStateTopic;
// MQTT topic to subscribe to to update the schedule
extern String greeniotyScheduleTopic;
// Publish interval in seconds (only publish status every publishInterval seconds)
extern int publishInterval;

// Callback function for incoming MQTT messages
void mqttCallback(char* topic, byte* payload, unsigned int length);

// MQTT client instance
extern WiFiClient espClient;
extern PubSubClient mqttClient;

// Function to reconnect to MQTT
bool reconnectMQTT();

// Function to publish the Greenioty status to the MQTT broker
bool publishGreeniotyStatus(const char* topic, int rawDistance, float waterLevel, float waterLevelPercent, lightState lights, pumpState pump);