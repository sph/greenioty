#pragma once

// Set Pins for US Sensor
#define PIN_US_SENSOR_RX 22
#define PIN_US_SENSOR_TX 23
#define PIN_US_SENSOR_POWER 21
#define PIN_US_SENSOR_CTRL PIN_US_SENSOR_TX // The Sensors RX pin is used to control the sensor mode (so it is the same as the ESPs TX pin)
// Baud Rate for US Sensor
#define US_SENSOR_BAUD_RATE 9600

#include <SoftwareSerial.h>

// Water tank Parameters
//#define WATER_TANK_HEIGHT 400.0f // mm
//#define WATER_TANK_AREA 150000.0f // mm^2
//#define WATER_LEVEL_MIN_THRESHOLD 10.0f // mm
//#define WATER_LEVEL_MAX 380.0f // mm

// Water Tank Class
class WaterTank{
private:
    float waterTankHeight; // mm
    float waterTankArea; // mm^2
    float waterLevelMinThreshold; // mm
    float waterLevelMax; // mm
    bool empty = false;
public:
    float waterLevel; // mm
    float waterLevelVolume; // liters
    float waterLevelPercentage; // %
    WaterTank(float height, float area, float threshold, float max);
    bool update(int distance);
    bool isEmpty();
};

// Ultrasonic Sensor Class
class UltrasonicSensor{
private:
    SoftwareSerial serial; // Direct member for SoftwareSerial

public:
    int distance; // Measured distance in mm (-1 if invalid)
    UltrasonicSensor(); // Constructor
    void initialize();
    bool readDistance();
    bool readDistanceStable();
    void powerOn();
    void powerOff();
};