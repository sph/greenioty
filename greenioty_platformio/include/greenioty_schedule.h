#pragma once

#include <Arduino.h>
#include "wifi_management.h"

// Time Synchronization Interval
#define SYNC_INTERVAL 86400U  // 24 hours (in seconds)

// Light schedule
extern int lightTurnOnHour; // Hour to turn on the light, 24-hour format
extern int lightTurnOffHour; // Hour to turn off the light, 24-hour format

// Pump schedule
extern int pumpInterval; // Interval to trigger the pump, in minutes
extern int pumpDuration; // Duration to keep the pump on, in minutes

// Struct to hold the current time
extern struct tm timeinfo;

// Hardware timer to synchronize the system time every 24 hours
extern hw_timer_t* sync_timer;
extern volatile bool syncTimeFlag;

// Hardware timer to turn on the pump
extern hw_timer_t* pump_timer;
extern volatile bool pumpFlag;

// Timer interrupt service routine for periodically synchronizing the system time
void IRAM_ATTR syncTimerCallback();

// Timer interrupt service routine for periodically triggering the pump
void IRAM_ATTR pumpCallback();

// Function to initialize the synchronization timer
void syncTimerInit();

// Function to initialize the pump timer
void pumpTimerInit();

// Function to set the pump timer interval
void pumpTimerSetInterval(uint64_t interval);

// Function to set the system time
bool setSystemTime();

// Function that returns the current time as a string
String getCurrentTimeString();

// Function to print the current time
void printCurrentTime();