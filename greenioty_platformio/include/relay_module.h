#pragma once

#include <Arduino.h>

#define PIN_RELAY_1 27
#define PIN_RELAY_2 26
#define PIN_RELAY_3 25
#define PIN_RELAY_4 33

#define LIGHT_RELAY 1
#define PUMP_RELAY 2

enum lightState{
    LIGHT_OFF,
    LIGHT_ON
};

enum pumpState{
    PUMP_OFF,
    PUMP_ON
};

extern lightState light_state;
extern pumpState pump_state;

class RelayModule{
    private:
        int relayPins[4] = {PIN_RELAY_1, PIN_RELAY_2, PIN_RELAY_3, PIN_RELAY_4};
    public:
        RelayModule();
        void turnOn(int relay);
        void turnOff(int relay);
        void turnOnLight();
        void turnOffLight();
        void turnOnPump();
        void turnOffPump();
};