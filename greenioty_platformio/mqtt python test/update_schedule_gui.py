import tkinter as tk
from tkinter import messagebox
import paho.mqtt.client as mqtt
import json

# Default values for MQTT configuration
DEFAULT_BROKER = "test.mosquitto.org"
DEFAULT_PORT = 1883
DEFAULT_TOPIC = "greenioty/set/schedule"
# Default values for schedule configuration
DEFAULT_LIGHT_ON_HOUR = 8
DEFAULT_LIGHT_OFF_HOUR = 20
DEFAULT_PUMP_INTERVAL = 60
DEFAULT_PUMP_DURATION = 15

# Function to publish MQTT message
def publish_message():
    try:
        # Retrieve values from input fields
        broker = broker_entry.get()
        port = int(port_entry.get())
        topic = topic_entry.get()
        username = username_entry.get()
        password = password_entry.get()
        pump_interval = int(pump_interval_entry.get())
        pump_duration = int(pump_duration_entry.get())
        light_turn_on_hour = int(light_on_entry.get())
        light_turn_off_hour = int(light_off_entry.get())

        # Validate values
        if not (0 <= light_turn_on_hour <= 23):
            raise ValueError("Light turn-on hour must be between 0 and 23.")
        if not (0 <= light_turn_off_hour <= 23):
            raise ValueError("Light turn-off hour must be between 0 and 23.")
        if pump_interval <= 0 or pump_duration <= 0:
            raise ValueError("Pump interval and duration must be positive integers.")

        # Prepare JSON payload
        update_message = {
            "pumpInterval": pump_interval,
            "pumpDuration": pump_duration,
            "lightTurnOnHour": light_turn_on_hour,
            "lightTurnOffHour": light_turn_off_hour
        }

        # Publish to MQTT
        client = mqtt.Client()

        # Set username and password if provided
        if username and password:
            client.username_pw_set(username, password)

        client.connect(broker, port, 60)
        payload = json.dumps(update_message)
        client.publish(topic, payload)
        client.disconnect()

        # Notify user
        messagebox.showinfo("Success", "Settings published successfully!")
    except ValueError as ve:
        messagebox.showerror("Input Error", str(ve))
    except Exception as e:
        messagebox.showerror("Error", f"An error occurred: {e}")

# GUI setup
root = tk.Tk()
root.title("MQTT Schedule Configurator")

# MQTT configuration fields
tk.Label(root, text="MQTT Broker:").grid(row=0, column=0, padx=10, pady=5, sticky="e")
broker_entry = tk.Entry(root)
broker_entry.insert(0, DEFAULT_BROKER)
broker_entry.grid(row=0, column=1, padx=10, pady=5)

tk.Label(root, text="MQTT Port:").grid(row=1, column=0, padx=10, pady=5, sticky="e")
port_entry = tk.Entry(root)
port_entry.insert(0, str(DEFAULT_PORT))
port_entry.grid(row=1, column=1, padx=10, pady=5)

tk.Label(root, text="MQTT Topic:").grid(row=2, column=0, padx=10, pady=5, sticky="e")
topic_entry = tk.Entry(root)
topic_entry.insert(0, DEFAULT_TOPIC)
topic_entry.grid(row=2, column=1, padx=10, pady=5)

tk.Label(root, text="MQTT Username:").grid(row=3, column=0, padx=10, pady=5, sticky="e")
username_entry = tk.Entry(root)
username_entry.grid(row=3, column=1, padx=10, pady=5)

tk.Label(root, text="MQTT Password:").grid(row=4, column=0, padx=10, pady=5, sticky="e")
password_entry = tk.Entry(root, show="*")
password_entry.grid(row=4, column=1, padx=10, pady=5)

# Schedule configuration fields
tk.Label(root, text="Pump Interval (minutes):").grid(row=5, column=0, padx=10, pady=5, sticky="e")
pump_interval_entry = tk.Entry(root)
pump_interval_entry.insert(0, str(DEFAULT_PUMP_INTERVAL))
pump_interval_entry.grid(row=5, column=1, padx=10, pady=5)

tk.Label(root, text="Pump Duration (minutes):").grid(row=6, column=0, padx=10, pady=5, sticky="e")
pump_duration_entry = tk.Entry(root)
pump_duration_entry.insert(0, str(DEFAULT_PUMP_DURATION))
pump_duration_entry.grid(row=6, column=1, padx=10, pady=5)

tk.Label(root, text="Light Turn-On Hour (24-hour):").grid(row=7, column=0, padx=10, pady=5, sticky="e")
light_on_entry = tk.Entry(root)
light_on_entry.insert(0, str(DEFAULT_LIGHT_ON_HOUR))
light_on_entry.grid(row=7, column=1, padx=10, pady=5)

tk.Label(root, text="Light Turn-Off Hour (24-hour):").grid(row=8, column=0, padx=10, pady=5, sticky="e")
light_off_entry = tk.Entry(root)
light_off_entry.insert(0, str(DEFAULT_LIGHT_OFF_HOUR))
light_off_entry.grid(row=8, column=1, padx=10, pady=5)

# Button Frame
button_frame = tk.Frame(root)
button_frame.grid(row=9, columnspan=2, pady=10)

# Submit button
tk.Button(button_frame, text="Publish Settings", command=publish_message).grid(row=0, column=0, padx=5)

# Quit button
tk.Button(button_frame, text="Quit", command=root.quit).grid(row=0, column=1, padx=5)

# Run the main event loop
root.mainloop()
