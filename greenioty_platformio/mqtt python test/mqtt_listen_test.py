"""This script listens for messages on a specific MQTT topic and prints them to the console."""

import paho.mqtt.client as mqtt

# Define the MQTT broker and topic
mqtt_broker = "test.mosquitto.org"
mqtt_port = 1883
mqtt_subscribe_topic = "greenioty/get/state"
#mqtt_subscribe_topic = "greenioty/set/schedule"

# Callback function when a message is received on the subscribed topic
def on_message(client, userdata, msg):
    # Print the received message to the console
    print(f"Received message on topic {msg.topic}: {msg.payload.decode()}")

# Create a new MQTT client
client = mqtt.Client()

# Assign the callback function for message reception
client.on_message = on_message

# Connect to the MQTT broker
client.connect(mqtt_broker, mqtt_port, 60)

# Subscribe to the topic
client.subscribe(mqtt_subscribe_topic)

# Start the MQTT client loop to listen for incoming messages
client.loop_forever()