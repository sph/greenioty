"""This script publishes user input to a specified MQTT topic."""

import paho.mqtt.client as mqtt

# Define the MQTT broker and topic
mqtt_broker = "test.mosquitto.org"
mqtt_port = 1883
mqtt_publish_topic = "greenioty/set/schedule"

# Create a new MQTT client
client = mqtt.Client()

# Connect to the MQTT broker
client.connect(mqtt_broker, mqtt_port, 60)

# Start the MQTT client loop in a separate thread
client.loop_start()

print("Enter your message to publish to the topic 'greenioty/set/schedule'. Type 'exit' to quit.")

# Loop to take user input and publish it to the MQTT topic
while True:
    message = input("Enter message: ")
    if message.lower() == "exit":
        break  # Exit the loop if the user types 'exit'
    
    # Publish the message to the topic
    client.publish(mqtt_publish_topic, message)
    print(f"Message published to {mqtt_publish_topic}: {message}")

# Stop the MQTT client loop and disconnect
client.loop_stop()
client.disconnect()
