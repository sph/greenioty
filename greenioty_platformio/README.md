# Greenioty ESP32 Controller

### Running Greenioty

* During power on (or reset), the switch needs to be toggled to `flash`. Otherwise the ESP32 cannot start properly.
* Make sure to switch back to `on` after power on.
* Make sure the Ultrasound Distance Sensor is plugged in. Otherwise the ESP32 does not initialize properly.
* Make sure power is plugged in.

### Setting up the ESP32

1) When powered up, you can press the config button to trigger the WiFi configuration portal. 
2) The ESP32 then opens a WiFi hotspot called `greenioty_config` to which you can connect. The password is `configMe:-)`. (These parameters are set and can be change in the firmware in the file `wifi_management.h`)
3) Once connected, the ESP32 prints the IP address of the portal to the serial monitor (make sure to be connected through USB and have the serial monitor open to catch the IP). On MacOS, the portal automatically pops open. On other systems, open a browser and enter the ESP32's IP address (the default seems to be `192.168.4.1`)
4) Click on 'Configure WiFi'

Here you can set:
* The WiFi SSID and Password that the ESP32 will use to connect to the internet.
* The MQTT server, port, topics, user and password.
* The light and pump schedules (can be changed later through MQTT).
* The frequency in which the ESP should publish a status update (can be changed later through MQTT).

### Updating the Light and Pump Schedule

The ESP32 is subscribed to the topic `greenioty/set/schedule` on the MQTT server `sph-prod.ethz.ch`.

The ESP32 expects a JSON string with the keys:
* `pumpInterval`
* `pumpDuration`
* `lightTurnOnHour`
* `lightTurnOffHour`
* `publishInterval`

to update the pump interval, the duration the pump is on, when the lights turn on and when the lights turn off respectively. Here is an example of such a JSON string:
`{"pumpInterval": 60, "pumpDuration": 15, "lightTurnOnHour": 8, "lightTurnOffHour": 20, "publishInterval": 60}`

You can use the python script `greenioty_mqtt_gui.py` to publish such a JSON string through MQTT. You only need the `paho-mqtt` library for this script. You can run it for example within a conda environment called greenioty:
* Create a conda environment (if you haven't already): `conda create --name greenioty`
* Activate the environment: `conda activate greenioty`
* Install the paho-mqtt library (if you haven't already): `pip install paho-mqtt`
* Navigate to the directory containing the python script and run it: `python greenioty_mqtt_gui.py`

Note that the pump timer will not be updated immediately, but only after it is triggered according to the previouse schedule or if the ESP32 is reset.

### Monitoring Greenioty

Similarely, the ESP32 publishes to the topic `greenioty/get/state` on the MQTT server `sph-prod.ethz.ch`. You can listen there for status updates. What the ESP32 publishes is set in the file `greenioty_mqtt.cpp`.

### Water Tank Parameters

The water tank dimensions, including minimum water level for the pump to work, are hardcoded into the firmware. If you need to change these, you can do that by changing these values at the top of the main file and flashing the firmware.

### Flashing the Firmware

The ESP32 can be flashed by directly connecting your computer through USB to the ESP32 dev board and using VSCode with the PlatformIO extension to flash the project.

**NOTE**: The relay module is powered directly through the ESP32 dev board and must be disconnected when flashing. Thus, before flashing, disconnect the relay module from the ESP32 dev board by flipping the switch to "flash" (TO DO: label the switch). Don't forget to flip the switch back once the code has successfully uploaded, otherwise the pump and lights won't work.

### Troubleshooting

#### Wifi Config Portal

The parameters that can be set here all have a maximum length. If you cannot enter a parameter in full, you might have to increse the size of the underlying `WiFiManagerParameter` object (last argument) that captures the user input. They are defined in the `wifi_management.cpp` file.

#### MQTT Payload

If you add more data to be sent through MQTT, you might need to increase the buffer size of the `PubSubClient` object (called mqttClient) by changing the input of the function call `mqttClient.setBufferSize()` in the setup function in the main file.

#### Saving and Retrieving Data from Non-Volatile Memory

The firmware uses the `Preferences` library to save parameters persistently between sessions. If you wish to save and retrieve more variables in non-volatile memory, note that the key (i.e. the name) under which you store the value has a maximum length (around 15 or 20 characters). If you exceed this maximum length, it won't work.

### Pin Map

![Pin Map](esp32_wiring/pin_map.png)

### Wiring Diagram

![Wiring Diagram](esp32_wiring/greenioty-wiring.drawio.png)