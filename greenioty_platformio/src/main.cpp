#include <Arduino.h>
#include "A02YYUW.h"
#include "greenioty_mqtt.h"
#include "wifi_management.h"
#include "relay_module.h"
#include "greenioty_schedule.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>

// Pin definitions
#define PIN_CFG_BUTTON 19 // Config button pin

// Water tank Parameters
#define WATER_TANK_HEIGHT 300.0f // distance from the bottom of the tank to the sensor in mm (box is 32cm deep, sensor is 2cm)
#define WATER_TANK_AREA 211875.0f // cross-sectional area in mm^2 (such that WATER_TANK_AREA * the measured water level in mm = water volume in mm^3)
#define WATER_LEVEL_MIN_THRESHOLD 100.0f // min water level in mm for the pump to still be submerged
#define WATER_LEVEL_MAX 220.0f // water level in mm when the tank is full

// Publish schedule
unsigned long lastPublishTime = 0;  // To keep track of the last publish time

// Flag to indicate if the config button has been pressed
volatile bool cfgButtonPressed = false;
volatile unsigned long lastCfgInterruptTime = 0;  // To handle debouncing

// Interrupt service routine for the config button press
void IRAM_ATTR handleConfigButtonPress() {
  unsigned long interruptTime = millis();
  // Check if enough time has passed since the last interrupt (debouncing)
  if (interruptTime - lastCfgInterruptTime > 2000) {  // 2000 ms debounce time
    cfgButtonPressed = true;  // Set the flag when the button is pressed
    lastCfgInterruptTime = interruptTime;  // Update the last config interrupt time
  }
}

// Create an instance of the ultrasound sensor
UltrasonicSensor us_sensor;

// Create a water tank object
WaterTank water_tank(WATER_TANK_HEIGHT, WATER_TANK_AREA, WATER_LEVEL_MIN_THRESHOLD, WATER_LEVEL_MAX);

// Create an instance of the relay module (to control the pump and light)
RelayModule relay_module;


void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);

  // Initialize ultrasound distance sensor
  us_sensor.initialize();

  // Initialize the config button pin
  pinMode(PIN_CFG_BUTTON, INPUT_PULLUP);
  // Attach an interrupt to the config button pin (falling edge trigger)
  attachInterrupt(digitalPinToInterrupt(PIN_CFG_BUTTON), handleConfigButtonPress, FALLING);

  // Initialize Wi-Fi configuration and connect to WiFi (This will also load the configuration parameters, e.g. mqtt credentials, light schedule etc., from previouse sessions)
  // If connecting for the first time, the configuration portal will be triggered
  WiFiConfigInit();

  // Set the system time
  if(!setSystemTime()){ // If the time cannot be set, set the flag to try again in the main loop
    syncTimeFlag = true;
  }

  // Initialize the synchronization timer to periodically synchronize the system time
  syncTimerInit();

  // Initialize the pump timer
  pumpTimerInit();

  mqttClient.setBufferSize(512);  // Set the buffer size for the MQTT client, increase this, if you add more data to the JSON message and it stops working.
  // Set MQTT server and callback
  mqttClient.setServer(mqttServer.c_str(), mqttPort);
  mqttClient.setCallback(mqttCallback);

}

void loop() {
  Serial.println("\nMain Loop...");

  // Read water distance from the sensor
  if (us_sensor.readDistanceStable()) {
    // Update the water tank values
    water_tank.update(us_sensor.distance);
  } else {
    Serial.println("Failed to read a stable distance.");
  }

  // Check if the MQTT client is connected
  if (!mqttClient.connected()) {
    Serial.println("Reconnecting to MQTT...");
    reconnectMQTT();
  }
  // Maintain the MQTT connection and handle incoming messages
  mqttClient.loop(); // Make sure this is called about once every second. If the loop gets too long, consider using a timer interrupt to call this function or call it multiple times throughout the loop.

  // Publish the Greenioty status
  if (millis() - lastPublishTime > publishInterval*1000) {  // Check if it is time to publish
    if(publishGreeniotyStatus(greeniotyStateTopic.c_str(), us_sensor.distance, water_tank.waterLevel, water_tank.waterLevelPercentage, light_state, pump_state)){
      lastPublishTime = millis();  // Update the last publish time
      Serial.println("Published Greenioty Status");
    }
    else{
      Serial.println("Failed to publish Greenioty Status");
    }
  }

  // Check if the config button has been pressed
  if (cfgButtonPressed) {
    Serial.println("Config Button Pressed!");

    // Trigger the configuration portal
    wifiManager.startConfigPortal(config_ssid, config_pass);
    // Set the system time after the configuration
    setSystemTime();

    cfgButtonPressed = false;  // Reset the flag
  }

  // Check if the pump timer interrupt has triggered
  if (pumpFlag) {
    Serial.println("Pump Timer Interrupt Triggered!");
    if(pump_state == PUMP_OFF){
      if(water_tank.isEmpty()){ // Check if the water tank is empty
        Serial.println("Water tank is empty. Cannot turn on the pump. Trying again in the next loop.");
        pumpTimerSetInterval(pumpInterval);  // Reset the pump timer
        pumpFlag = true;  // Do not reset the flag, try again in the next loop
      }
      else{
        Serial.println("Turning on the pump.");
        relay_module.turnOnPump();  // Turn on the pump
        pump_state = PUMP_ON;  // Set the pump state
        // Set pump on duration
        pumpTimerSetInterval(pumpDuration);  // Pump will be on for pumpDuration minutes
        pumpFlag = false;  // Clear the flag
      }
    }
    else if(pump_state == PUMP_ON){
      Serial.println("Turning off the pump.");
      relay_module.turnOffPump();  // Turn off the pump
      pump_state = PUMP_OFF;  // Clear the pump state
      // Set pump off duration
      pumpTimerSetInterval((uint64_t)(pumpInterval-pumpDuration));  // Set the pump timer to the remaining minutes of the pumpInterval now that pumpDuration has elapsed
      pumpFlag = false;  // Clear the flag
    }
  }

  // Polling to ensure the light schedule is followed
  if (getLocalTime(&timeinfo)) {
    //Serial.println("Checking Light Schedule...");
    if(lightTurnOnHour < lightTurnOffHour){ // Expected case: Light on during the day
      // Check if its time to turn on the light and the light is off
      if(light_state == LIGHT_OFF and (timeinfo.tm_hour >= lightTurnOnHour and timeinfo.tm_hour < lightTurnOffHour)){
        Serial.println("Turning on the lights.");
        relay_module.turnOnLight(); // Turn on the light
        light_state = LIGHT_ON;
      // Check if its time to turn off the light and the light is on
      } else if(light_state == LIGHT_ON and (timeinfo.tm_hour < lightTurnOnHour or timeinfo.tm_hour >= lightTurnOffHour)){
        Serial.println("Turning off the lights.");
        relay_module.turnOffLight(); // Turn off the light
        light_state = LIGHT_OFF;
      }
    }
    else if(lightTurnOffHour < lightTurnOnHour){ // Special case: Light on during the night
      // Check if its time to turn on the light and the light is off
      if(light_state == LIGHT_OFF and (timeinfo.tm_hour >= lightTurnOnHour or timeinfo.tm_hour < lightTurnOffHour)){
        Serial.println("Turning on the lights.");
        relay_module.turnOnLight(); // Turn on the light
        light_state = LIGHT_ON;
      // Check if its time to turn off the light and the light is on
      } else if(light_state == LIGHT_ON and (timeinfo.tm_hour < lightTurnOnHour and timeinfo.tm_hour >= lightTurnOffHour)){
        Serial.println("Turning off the lights.");
        relay_module.turnOffLight(); // Turn off the light
        light_state = LIGHT_OFF;
      }
    }
  }

  // Check if the synchronization timer interrupt has triggered
  if (syncTimeFlag) {
    Serial.println("Synchronizing System Time...");
    if(setSystemTime()){ // If the time is set successfully
      Serial.print(" Success!");
      syncTimeFlag = false;  // Clear the flag
    }
    else{
      Serial.println("Failed to synchronize the system time. Trying again in the next loop.");
    }
  }

}


