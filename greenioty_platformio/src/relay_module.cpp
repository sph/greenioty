#include "relay_module.h"

// Light state
lightState light_state = LIGHT_OFF;
// Pump state
pumpState pump_state = PUMP_OFF;

// Constructor initializes the relay pins
RelayModule::RelayModule() {
    pinMode(PIN_RELAY_1, OUTPUT);
    pinMode(PIN_RELAY_2, OUTPUT);
    pinMode(PIN_RELAY_3, OUTPUT);
    pinMode(PIN_RELAY_4, OUTPUT);
    // Turn off all relays
    digitalWrite(PIN_RELAY_1, HIGH);
    digitalWrite(PIN_RELAY_2, HIGH);
    digitalWrite(PIN_RELAY_3, HIGH);
    digitalWrite(PIN_RELAY_4, HIGH);
}

// Turn on the specified relay
void RelayModule::turnOn(int relay) {
    if (relay >= 1 && relay <= 4) {
        digitalWrite(relayPins[relay - 1], LOW);
    }
}

// Turn off the specified relay
void RelayModule::turnOff(int relay) {
    if (relay >= 1 && relay <= 4) {
        digitalWrite(relayPins[relay - 1], HIGH);
    }
}

// Turn on the light relay
void RelayModule::turnOnLight() {
    turnOn(LIGHT_RELAY);
}

// Turn off the light relay
void RelayModule::turnOffLight() {
    turnOff(LIGHT_RELAY);
}

// Turn on the pump relay
void RelayModule::turnOnPump() {
    turnOn(PUMP_RELAY);
}

// Turn off the pump relay
void RelayModule::turnOffPump() {
    turnOff(PUMP_RELAY);
}