#include "greenioty_schedule.h"

// Light schedule, values are set in the WiFi configuration portal
int lightTurnOnHour; // Hour to turn on the light, 24-hour format
int lightTurnOffHour; // Hour to turn off the light, 24-hour format

// Pump schedule, values are set in the WiFi configuration portal
int pumpInterval; //Interval to trigger the pump, in minutes
int pumpDuration; // Duration to keep the pump on, in minutes

// Struct to hold the current time
struct tm timeinfo;

// Hardware timer to synchronize the system time every 24 hours
hw_timer_t* sync_timer = NULL;
volatile bool syncTimeFlag = false;

// Hardware timer to trigger the pump
hw_timer_t* pump_timer = NULL;
volatile bool pumpFlag = false;

// Timer interrupt service routine for periodically synchronizing the system time
void IRAM_ATTR syncTimerCallback() {
  syncTimeFlag = true;  // Set the flag for the main loop
}

// Timer interrupt service routine for periodically turning on the pump
void IRAM_ATTR pumpCallback() {
  pumpFlag = true;  // Set the flag for the main loop
}

// Function to initialize the synchronization timer
void syncTimerInit(){
  // Create a hardware timer to synchronize the system time every 24 hours
  sync_timer = timerBegin(0, 80, true);  // Timer 0, prescaler 80 (-> ticks in us), count up
  timerAttachInterrupt(sync_timer, &syncTimerCallback, true);
  uint64_t interval = (uint64_t)(SYNC_INTERVAL*1000000);  // SYNC_INTERVAL seconds
  timerAlarmWrite(sync_timer, interval, true); 
  timerAlarmEnable(sync_timer);
}

// Function to initialize the timer to turn on the pump
void pumpTimerInit(){
  // Create a hardware timer to turn on the pump every pumpInterval minutes
  pump_timer = timerBegin(1, 80, true);  // Timer 1, prescaler 80 (-> ticks in us), count up
  timerAttachInterrupt(pump_timer, &pumpCallback, true);
  uint64_t interval = (uint64_t)pumpInterval*60000000; // pumpInterval * 60'000'000 us = pumpInterval minutes
  timerAlarmWrite(pump_timer, interval, true);
  timerAlarmEnable(pump_timer);
}

void pumpTimerSetInterval(uint64_t interval){
  timerAlarmDisable(pump_timer); // Disable the pump timer
  timerWrite(pump_timer, 0);  // Reset the pump timer
  uint64_t interval_us = (uint64_t)interval*60000000; // interval * 60'000'000 us = interval minutes
  timerAlarmWrite(pump_timer, interval_us, true);
  timerAlarmEnable(pump_timer);  // Enable the pump timer
}

// Function to set the system time
bool setSystemTime(){
  // Check if Wi-Fi is connected
  if (WiFi.status() != WL_CONNECTED){
    // Try to reconnect to Wi-Fi
    if(!wifiManager.autoConnect(config_ssid)){
      Serial.println("Cannot set time without Wi-Fi connection.");
      return false;
    }
  }
  // If Wi-Fi is connected, proceed to set the system time
  // Timezone information for Central European Time (CET/CEST)
  const char* timeZone = "CET-1CEST,M3.5.0,M10.5.0";
  
  // Configure time with NTP server and timezone
  configTime(0, 0, "pool.ntp.org", "time.nist.gov");
  setenv("TZ", timeZone, 1);  // Set the environment variable for timezone
  tzset();                    // Apply the timezone

  // Wait for the time to be set
  //Serial.println("Waiting for time synchronization...");
  struct tm timeinfo;
  const unsigned long timeoutMs = 10000;  // Timeout for time synchronization (10 seconds)
  unsigned long startTime = millis();
  while (!getLocalTime(&timeinfo)) {
    if (millis() - startTime > timeoutMs) {
      Serial.println("Failed to synchronize time: Timeout reached.");
      return false;  // Return false if synchronization fails
    }
    Serial.print(".");
    delay(1000);
  }
  //Serial.println("Time synchronized");

  // Display the current time
  char timeString[64];
  strftime(timeString, sizeof(timeString), "%Y-%m-%d %H:%M:%S", &timeinfo);
  Serial.print("Current time: ");
  Serial.println(timeString);
  return true;
}

// Function that returns the current time as a string
String getCurrentTimeString(){
  struct tm timeinfo;
  if (getLocalTime(&timeinfo)) {
    char timeString[64];
    strftime(timeString, sizeof(timeString), "%Y-%m-%d %H:%M:%S", &timeinfo);
    return String(timeString);
  } else {
    return "Error";
  }
}

// Function to print the current time
void printCurrentTime() {
  struct tm timeinfo;

  // Get the current time
  if (getLocalTime(&timeinfo)) {
    char timeString[64];
    strftime(timeString, sizeof(timeString), "%Y-%m-%d %H:%M:%S", &timeinfo);
    Serial.print("System time: ");
    Serial.println(timeString);
  } else {
    Serial.println("Failed to retrieve system time.");
  }
}