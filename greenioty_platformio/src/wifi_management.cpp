#include "wifi_management.h"

// Create a Preferences object to store the configuration parameters persistently
Preferences greeniotyPrefs;

// WiFi Configuration Portal Parameters
const char* config_ssid = "greenioty_config";
const char* config_pass = "configMe:-)";

// Create global instances of WiFiManager
WiFiManager wifiManager;

// Create custom parameters for the Wi-Fi configuration
WiFiManagerParameter custom_light_on_hour("light_on_hour", "Light On Hour", "8", 2);  // Default hour
WiFiManagerParameter custom_light_off_hour("light_off_hour", "Light Off Hour", "20", 2);  // Default hour
WiFiManagerParameter custom_pump_interval("pump_interval", "Pump Interval (minutes)", "60", 4);  // Default interval
WiFiManagerParameter custom_pump_duration("pump_duration", "Pump Duration (minutes)", "15", 4);  // Default duration
WiFiManagerParameter custom_mqtt_server("mqtt_server", "MQTT Server", "sph-prod.ethz.ch", 40);  // Default IP
WiFiManagerParameter custom_mqtt_port("mqtt_port", "MQTT Port", "1883", 5);  // Default port
WiFiManagerParameter custom_mqtt_get_state_topic("mqtt_get_state_topic", "MQTT Get State Topic", "greenioty/get/state", 40);  // Default topic
WiFiManagerParameter custom_mqtt_set_schedule_topic("mqtt_set_schedule_topic", "MQTT Set Schedule Topic", "greenioty/set/schedule", 40);  // Default topic
WiFiManagerParameter custom_mqtt_user("mqtt_user", "MQTT User", "NONE", 20);  // Default user
WiFiManagerParameter custom_mqtt_pass("mqtt_pass", "MQTT Password", "NONE", 40);  // Default password
WiFiManagerParameter custom_mqtt_publish_interval("mqtt_pub_interval", "MQTT Publish Interval (seconds)", "60", 5);  // Default interval

// Callback function to handle the Wi-Fi configuration parameters
void saveConfigCallback(){
  Serial.println("Saving Configuration...");
  // Save the custom parameters to global variables
  lightTurnOnHour = atoi(custom_light_on_hour.getValue());
  lightTurnOffHour = atoi(custom_light_off_hour.getValue());
  pumpInterval = atoi(custom_pump_interval.getValue());
  pumpDuration = atoi(custom_pump_duration.getValue());
  mqttServer = custom_mqtt_server.getValue();
  mqttPort = atoi(custom_mqtt_port.getValue());
  greeniotyStateTopic = custom_mqtt_get_state_topic.getValue();
  greeniotyScheduleTopic = custom_mqtt_set_schedule_topic.getValue();
  // Check if the MQTT user and password are set
  if(strcmp(custom_mqtt_user.getValue(), "NONE") == 0){
    mqttUser = "";
    mqttPassword = "";
  }
  else{
    mqttUser = custom_mqtt_user.getValue();
    mqttPassword = custom_mqtt_pass.getValue();
  }
  publishInterval = atoi(custom_mqtt_publish_interval.getValue());
  if (publishInterval < 0) { // Make sure interval is positive
    publishInterval = 60;  // Default publish interval
  }

  // Save the custom parameters to non-volatile memory
  greeniotyPrefs.begin("greeniotyConfig", false);
  greeniotyPrefs.putInt("lightOnHour", lightTurnOnHour);
  greeniotyPrefs.putInt("lightOffHour", lightTurnOffHour);
  greeniotyPrefs.putInt("pumpInterval", pumpInterval);
  greeniotyPrefs.putInt("pumpDuration", pumpDuration);
  greeniotyPrefs.putString("mqttServer", mqttServer);
  greeniotyPrefs.putInt("mqttPort", mqttPort);
  greeniotyPrefs.putString("StateTopic", greeniotyStateTopic);
  greeniotyPrefs.putString("ScheduleTopic", greeniotyScheduleTopic);
  greeniotyPrefs.putString("mqttUser", mqttUser);
  greeniotyPrefs.putString("mqttPassword", mqttPassword);
  greeniotyPrefs.putInt("publishInterval", publishInterval);
  greeniotyPrefs.end();
}

// Function to load the configuration parameters from the non-volatile memory (Preferences)
void loadConfig(){
  greeniotyPrefs.begin("greeniotyConfig", true);
  // Test if the configuration parameters exist in the Preferences
  if(!greeniotyPrefs.isKey("lightOnHour")){
    Serial.println("Configuration not found. Set them through the WiFi Configuration Portal.");
    return;
  }
  // Load the configuration parameters
  lightTurnOnHour = greeniotyPrefs.getInt("lightOnHour");
  lightTurnOffHour = greeniotyPrefs.getInt("lightOffHour");
  pumpInterval = greeniotyPrefs.getInt("pumpInterval");
  pumpDuration = greeniotyPrefs.getInt("pumpDuration");
  mqttServer = greeniotyPrefs.getString("mqttServer");
  mqttPort = greeniotyPrefs.getInt("mqttPort");
  greeniotyStateTopic = greeniotyPrefs.getString("StateTopic");
  greeniotyScheduleTopic = greeniotyPrefs.getString("ScheduleTopic");
  mqttUser = greeniotyPrefs.getString("mqttUser");
  mqttPassword = greeniotyPrefs.getString("mqttPassword");
  publishInterval = greeniotyPrefs.getInt("publishInterval");
  greeniotyPrefs.end();

  Serial.println("Configuration Loaded.");
}

// Function to update the schedule parameters in the non-volatile memory
void updateSchedulePersistant(){
  greeniotyPrefs.begin("greeniotyConfig", false);
  greeniotyPrefs.putInt("lightOnHour", lightTurnOnHour);
  greeniotyPrefs.putInt("lightOffHour", lightTurnOffHour);
  greeniotyPrefs.putInt("pumpInterval", pumpInterval);
  greeniotyPrefs.putInt("pumpDuration", pumpDuration);
  greeniotyPrefs.putInt("publishInterval", publishInterval);
  greeniotyPrefs.end();
  Serial.println("Schedule updated!");
}

// Function to initialize the Wi-Fi configuration
void WiFiConfigInit(){
  // Set callback for saving parameters
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  // Set a timeout for the configuration portal
  wifiManager.setConfigPortalTimeout(300);

  // Add custom parameters to the Wi-FiManager
  wifiManager.addParameter(&custom_light_on_hour);
  wifiManager.addParameter(&custom_light_off_hour);
  wifiManager.addParameter(&custom_pump_interval);
  wifiManager.addParameter(&custom_pump_duration);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_get_state_topic);
  wifiManager.addParameter(&custom_mqtt_set_schedule_topic);
  wifiManager.addParameter(&custom_mqtt_user);
  wifiManager.addParameter(&custom_mqtt_pass);
  wifiManager.addParameter(&custom_mqtt_publish_interval);

  // Try to connect to Wi-Fi using saved credentials, if any
  if (!wifiManager.autoConnect(config_ssid, config_pass)) {
    Serial.println("Failed to connect to Wi-Fi and entered configuration mode.");
  } else {
    // Successfully connected to Wi-Fi
    Serial.println("Connected to Wi-Fi");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
    // Load the configuration parameters
    loadConfig();
  }

}