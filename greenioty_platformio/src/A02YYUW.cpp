#include "A02YYUW.h"

// Sensor defines
#define MAX_STARTFLAG_WAIT 10
#define MAX_WATCHDOG_COUNTER 100

// Water Tank Class
// Constructor initializes the water tank parameters
WaterTank::WaterTank(float height, float area, float threshold, float max) 
    : waterLevel(-1), waterLevelVolume(-1), waterLevelPercentage(-1), waterLevelMinThreshold(threshold), waterTankHeight(height), waterTankArea(area), waterLevelMax(max), empty(true) {}

// Update method calculates the water level, volume, and percentage based on the distance measured by the ultrasound sensor
// distance is in mm, -1 if invalid
bool WaterTank::update(int distance) {
    if (distance == -1) { // Invalid distance
        return false;
    }
    waterLevel = waterTankHeight - distance; // Calculate the water level in mm
    waterLevelVolume = waterLevel * waterTankArea / 1000000; // Calculate the water volume in liters
    waterLevelPercentage = ((waterLevel-waterLevelMinThreshold) / (waterLevelMax-waterLevelMinThreshold)) * 100; // Calculate the water level percentage
    waterLevelPercentage= round(waterLevelPercentage * 100) / 100.0; // Round to two decimal places
    if (waterLevel < waterLevelMinThreshold) { // Check if the tank is empty
        empty = true;
    } else {
        empty = false;
    }
    return true;
}

// Check if the water tank is empty
bool WaterTank::isEmpty() {
    return empty;
}


// Sensor Class
// Constructor initializes the SoftwareSerial instance with predefined pins
UltrasonicSensor::UltrasonicSensor() 
    : serial(PIN_US_SENSOR_RX, PIN_US_SENSOR_TX), distance(-1) {}

// Begin method initializes the serial communication and the power and control pins
void UltrasonicSensor::initialize() {
    serial.begin(US_SENSOR_BAUD_RATE);
    pinMode(PIN_US_SENSOR_POWER, OUTPUT);
    pinMode(PIN_US_SENSOR_CTRL, OUTPUT);
}

// Power on the sensor
void UltrasonicSensor::powerOn() {
    digitalWrite(PIN_US_SENSOR_POWER, HIGH); // Turn on the sensor
    delay(100); // Allow time for the sensor to stabilize
}

// Power off the sensor
void UltrasonicSensor::powerOff() {
    digitalWrite(PIN_US_SENSOR_POWER, LOW); // Turn off the sensor
}

// Reads the distance measured by the ultrasound sensor
// PRE: The sensor is powered on
bool UltrasonicSensor::readDistance() {
    uint8_t response[3] = {};
    serial.flush();

    // Request processed sensor value
    digitalWrite(PIN_US_SENSOR_TX, HIGH);
    delay(300); // Wait the response time of the sensor

    // Wait for the start flag (should arrive after max 3 bytes)
    for (uint8_t i = 0; i < MAX_STARTFLAG_WAIT && (serial.read() != 0xff); ++i) {}

    // Read the three bytes after the start flag
    for (int i = 0; i < 3; i++) {
        response[i] = serial.read();
    }

    // Calculate the checksum
    uint8_t checksum = (0xff + response[0] + response[1]);
    if (checksum == response[2]) { // If the transmission is valid
        distance = (response[0] << 8) + response[1]; // Extract the distance in mm
        return true;
    } else {
        return false;
    }
}

// Reads a stable distance value from the sensor by taking three consecutive similar readings
bool UltrasonicSensor::readDistanceStable() {
    powerOn(); // Ensure the sensor is powered on
    int prev_distance = 0;
    uint8_t stable_counter = 0;
    uint8_t watchdog_counter = 0;

    while (stable_counter < 3) { // Measure distance until there are three consecutive similar readings
        if (readDistance()) {
            if ((distance < prev_distance + 5) && (distance > prev_distance - 5)) {
                ++stable_counter;
            }
            prev_distance = distance;
        }

        if (++watchdog_counter > MAX_WATCHDOG_COUNTER) { // ERROR if failed to read stable value for more than 100 tries
            distance = -1;
            powerOff(); // Turn the sensor off before returning
            return false;
        }
    }
    powerOff(); // Turn the sensor off again
    return true;
}