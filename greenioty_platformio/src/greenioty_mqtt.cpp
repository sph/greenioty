#include "greenioty_mqtt.h"

// MQTT Parameters
String mqttServer; // Set in the configuration portal
int mqttPort; // Set in the configuration portal
String mqttUser; // Set in the configuration portal
String mqttPassword; // Set in the configuration portal
const char* mqttClientID = "ESP32Greenioty"; // Client ID for MQTT
// MQTT topic to publish water level, pump status, and light status
String greeniotyStateTopic; // Set in the configuration portal
// MQTT topic to subscribe to to update the schedule
String greeniotyScheduleTopic; // Set in the configuration portal
int publishInterval; // Set in the configuration portal

// Callback function for incoming MQTT messages (needs to be declared before the mqttClient instance)
void mqttCallback(char* topic, byte* payload, unsigned int length) {
 
  // Print the incoming message
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // If the topic is the schedule topic, parse the incoming message and update the schedule 
  if (strcmp(topic, greeniotyScheduleTopic.c_str()) == 0) {
    // Create a buffer for the payload
    char updateMessage[length + 1];
    memcpy(updateMessage, payload, length);
    updateMessage[length] = '\0'; // Null-terminate the string

    // Parse the JSON string
    JsonDocument scheduleDoc; // Adjust size based on JSON complexity
    DeserializationError error = deserializeJson(scheduleDoc, updateMessage);

    if (error) {
      Serial.print("JSON parsing failed: ");
      Serial.println(error.f_str());
      return;
    }

    // Update variables if the keys are present
    if (scheduleDoc["pumpInterval"].is<int>()) {
      pumpInterval = scheduleDoc["pumpInterval"];
      Serial.print("Updated pumpInterval: ");
      Serial.println(pumpInterval);
      // The new pump interval will take effect the next time the pump is triggered according to the old pumpInterval
    }
    if (scheduleDoc["pumpDuration"].is<int>()) {
      pumpDuration = scheduleDoc["pumpDuration"];
      Serial.print("Updated pumpDuration: ");
      Serial.println(pumpDuration);
      // The new pump duration will take effect the next time the pump is triggered according to the old schedule
    }
    if (scheduleDoc["lightTurnOnHour"].is<int>()) {
      lightTurnOnHour = scheduleDoc["lightTurnOnHour"];
      Serial.print("Updated lightTurnOnHour: ");
      Serial.println(lightTurnOnHour);
    }
    if (scheduleDoc["lightTurnOffHour"].is<int>()) {
      lightTurnOffHour = scheduleDoc["lightTurnOffHour"];
      Serial.print("Updated lightTurnOffHour: ");
      Serial.println(lightTurnOffHour);
    }
    if (scheduleDoc["publishInterval"].is<int>()) {
      publishInterval = scheduleDoc["publishInterval"];
      Serial.print("Updated publishInterval: ");
      Serial.println(publishInterval);
    }

    // Save the schedule to non-volatile memory
    Serial.print("Updating the schedule in non-volatile memory...");
    updateSchedulePersistant();
  }
  
}

WiFiClient espClient;
PubSubClient mqttClient(espClient);

bool reconnectMQTT() {
  // Check if Wi-Fi is connected
  if (WiFi.status() != WL_CONNECTED){
    // Try to reconnect to Wi-Fi
    if(!wifiManager.autoConnect(config_ssid)){
      Serial.println("Cannot reconnect to MQTT without WiFi.");
      return false;
    }
  }
  // If Wi-Fi is connected, proceed to reconnect to MQTT
  if (!mqttClient.connected()) {
    Serial.print("Connecting to MQTT...");
    if (mqttClient.connect(mqttClientID, mqttUser.c_str(), mqttPassword.c_str())) {
      Serial.println("connected");
      mqttClient.subscribe(greeniotyScheduleTopic.c_str());
      return true;
    } else {
      Serial.print("failed with state ");
      Serial.println(mqttClient.state());
      return false;
    }
  }
  return true;
}

bool publishGreeniotyStatus(const char* topic, int rawDistance, float waterLevel, float waterLevelPercent, lightState lights, pumpState pump){
  // Prepare the payload
  // Create JSON document
  JsonDocument greeniotyStatusDoc;
  greeniotyStatusDoc["did"] = mqttClientID; // device ID
  greeniotyStatusDoc["waterDistance"] = rawDistance; // measured distance between sensor and water surface in mm
  greeniotyStatusDoc["waterLevel"] = waterLevel; // water level in mm
  greeniotyStatusDoc["waterLevelPercent"] = waterLevelPercent; // water level in percentage
  if(lights == LIGHT_ON){
    greeniotyStatusDoc["lightState"] = "on";
  } else {
    greeniotyStatusDoc["lightState"] = "off";
  }
  if(pump == PUMP_ON){
    greeniotyStatusDoc["pumpState"] = "on";
  } else {
    greeniotyStatusDoc["pumpState"] = "off";
  }
  greeniotyStatusDoc["pumpInterval"] = pumpInterval; // pump interval in minutes
  greeniotyStatusDoc["pumpDuration"] = pumpDuration; // pump duration in minutes
  greeniotyStatusDoc["lightTurnOnHour"] = lightTurnOnHour; // light on hour in 24-hour format
  greeniotyStatusDoc["lightTurnOffHour"] = lightTurnOffHour; // light off hour in 24-hour format
  greeniotyStatusDoc["timestamp"] = getCurrentTimeString(); // current time

  // Serialize JSON document to a string
  char greeniotyPayload[512];
  serializeJson(greeniotyStatusDoc, greeniotyPayload);
  // MQTT
  if(!mqttClient.connected()){// Check if the MQTT client is connected
    Serial.println("MQTT client not connected, reconnecting...");
    if(!reconnectMQTT()){ // If not, reconnect to MQTT
      Serial.println("Reconnecting failed. Cannot publish Greenioty Status.");
      return false;
    }
  }
  // Publish the payload and return whether the publish was successful
  return mqttClient.publish(topic, greeniotyPayload);
}