import tkinter as tk
from tkinter import messagebox, scrolledtext
import paho.mqtt.client as mqtt
import json
import threading

# Default values for MQTT configuration
DEFAULT_BROKER = "sph-prod.ethz.ch"
DEFAULT_PORT = 1883
DEFAULT_PUBLISH_TOPIC = "greenioty/set/schedule"
DEFAULT_SUBSCRIBE_TOPIC = "greenioty/get/state"
# Default values for schedule configuration
DEFAULT_LIGHT_ON_HOUR = 8
DEFAULT_LIGHT_OFF_HOUR = 20
DEFAULT_PUMP_INTERVAL = 60
DEFAULT_PUMP_DURATION = 15
DEFAULT_PUBLISH_INTERVAL = 60

# Global MQTT client
mqtt_client = None

# Function to handle incoming messages
def on_message(client, userdata, message):
    try:
        payload = message.payload.decode("utf-8")
        topic = message.topic
        display_area.insert(tk.END, f"Topic: {topic}\nMessage: {payload}\n\n")
        display_area.see(tk.END)  # Scroll to the latest message
    except Exception as e:
        display_area.insert(tk.END, f"Error processing message: {e}\n\n")

# Function to connect and subscribe to the topic
def connect_mqtt():
    global mqtt_client
    try:
        broker = broker_entry.get()
        port = int(port_entry.get())
        subscribe_topic = subscribe_topic_entry.get()
        username = username_entry.get()
        password = password_entry.get()

        mqtt_client = mqtt.Client()
        if username and password:
            mqtt_client.username_pw_set(username, password)
        mqtt_client.on_message = on_message
        mqtt_client.connect(broker, port, 60)
        mqtt_client.subscribe(subscribe_topic)
        mqtt_client.loop_start()
        display_area.insert(tk.END, f"Connected to {broker} on port {port} and subscribed to {subscribe_topic}\n\n")
    except Exception as e:
        messagebox.showerror("Error", f"Failed to connect to MQTT broker: {e}")

# Function to publish MQTT message
def publish_message():
    try:
        broker = broker_entry.get()
        port = int(port_entry.get())
        publish_topic = publish_topic_entry.get()
        username = username_entry.get()
        password = password_entry.get()
        pump_interval = int(pump_interval_entry.get())
        pump_duration = int(pump_duration_entry.get())
        light_turn_on_hour = int(light_on_entry.get())
        light_turn_off_hour = int(light_off_entry.get())
        publish_interval = int(publish_interval_entry.get())

        if not (0 <= light_turn_on_hour <= 23):
            raise ValueError("Light turn-on hour must be between 0 and 23.")
        if not (0 <= light_turn_off_hour <= 23):
            raise ValueError("Light turn-off hour must be between 0 and 23.")
        if pump_interval <= 0 or pump_duration <= 0:
            raise ValueError("Pump interval and duration must be positive integers.")

        update_message = {
            "pumpInterval": pump_interval,
            "pumpDuration": pump_duration,
            "lightTurnOnHour": light_turn_on_hour,
            "lightTurnOffHour": light_turn_off_hour,
            "publishInterval": publish_interval
        }

        if not mqtt_client:
            connect_mqtt()

        payload = json.dumps(update_message)
        mqtt_client.publish(publish_topic, payload)
        messagebox.showinfo("Success", "Settings published successfully!")
    except ValueError as ve:
        messagebox.showerror("Input Error", str(ve))
    except Exception as e:
        messagebox.showerror("Error", f"An error occurred: {e}")

# GUI setup
root = tk.Tk()
root.title("MQTT Schedule Configurator")

# Configure grid weights for resizing
root.grid_columnconfigure(1, weight=1)
root.grid_rowconfigure(11, weight=1)

# MQTT configuration fields
tk.Label(root, text="MQTT Broker:").grid(row=0, column=0, padx=10, pady=5, sticky="e")
broker_entry = tk.Entry(root)
broker_entry.insert(0, DEFAULT_BROKER)
broker_entry.grid(row=0, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="MQTT Port:").grid(row=1, column=0, padx=10, pady=5, sticky="e")
port_entry = tk.Entry(root)
port_entry.insert(0, str(DEFAULT_PORT))
port_entry.grid(row=1, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="Publish Topic:").grid(row=2, column=0, padx=10, pady=5, sticky="e")
publish_topic_entry = tk.Entry(root)
publish_topic_entry.insert(0, DEFAULT_PUBLISH_TOPIC)
publish_topic_entry.grid(row=2, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="Subscribe Topic:").grid(row=3, column=0, padx=10, pady=5, sticky="e")
subscribe_topic_entry = tk.Entry(root)
subscribe_topic_entry.insert(0, DEFAULT_SUBSCRIBE_TOPIC)
subscribe_topic_entry.grid(row=3, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="MQTT Username:").grid(row=4, column=0, padx=10, pady=5, sticky="e")
username_entry = tk.Entry(root)
username_entry.grid(row=4, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="MQTT Password:").grid(row=5, column=0, padx=10, pady=5, sticky="e")
password_entry = tk.Entry(root, show="*")
password_entry.grid(row=5, column=1, padx=10, pady=5, sticky="we")

# Schedule configuration fields
tk.Label(root, text="Pump Interval (minutes):").grid(row=6, column=0, padx=10, pady=5, sticky="e")
pump_interval_entry = tk.Entry(root)
pump_interval_entry.insert(0, str(DEFAULT_PUMP_INTERVAL))
pump_interval_entry.grid(row=6, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="Pump Duration (minutes):").grid(row=7, column=0, padx=10, pady=5, sticky="e")
pump_duration_entry = tk.Entry(root)
pump_duration_entry.insert(0, str(DEFAULT_PUMP_DURATION))
pump_duration_entry.grid(row=7, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="Light Turn-On Hour (24-hour):").grid(row=8, column=0, padx=10, pady=5, sticky="e")
light_on_entry = tk.Entry(root)
light_on_entry.insert(0, str(DEFAULT_LIGHT_ON_HOUR))
light_on_entry.grid(row=8, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="Light Turn-Off Hour (24-hour):").grid(row=9, column=0, padx=10, pady=5, sticky="e")
light_off_entry = tk.Entry(root)
light_off_entry.insert(0, str(DEFAULT_LIGHT_OFF_HOUR))
light_off_entry.grid(row=9, column=1, padx=10, pady=5, sticky="we")

tk.Label(root, text="Publish Interval (Seconds):").grid(row=10, column=0, padx=10, pady=5, sticky="e")
publish_interval_entry = tk.Entry(root)
publish_interval_entry.insert(0, str(DEFAULT_PUBLISH_INTERVAL))
publish_interval_entry.grid(row=10, column=1, padx=10, pady=5, sticky="we")

# Display for incoming messages
tk.Label(root, text="Incoming MQTT Messages:").grid(row=11, column=0, columnspan=2, pady=5)
display_area = scrolledtext.ScrolledText(root, height=10, state='normal')
display_area.grid(row=11, column=0, columnspan=2, padx=10, pady=5, sticky="nsew")

# Button Frame
button_frame = tk.Frame(root)
button_frame.grid(row=12, columnspan=2, pady=10)

tk.Button(button_frame, text="Publish Settings", command=publish_message).grid(row=0, column=0, padx=5)
tk.Button(button_frame, text="Connect", command=connect_mqtt).grid(row=0, column=1, padx=5)
tk.Button(button_frame, text="Quit", command=root.quit).grid(row=0, column=2, padx=5)

# Run the main event loop
root.mainloop()
